import React from "react";
import NavigationBar from "../components/NavigationBar";
import Body from "../components/Body";

const HomePage = () => {
  return (
    <div>
      <NavigationBar />
      <Body />
    </div>
  );
};

export default HomePage;
