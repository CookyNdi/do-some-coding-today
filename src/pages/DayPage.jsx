import React from "react";
import NavigationBar from "../components/NavigationBar";
import BodyDay from "../components/BodyDay";

const DayPage = () => {
  return (
    <div>
      <NavigationBar />
      <BodyDay />
    </div>
  );
};

export default DayPage;
