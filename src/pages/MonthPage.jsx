import React from "react";
import NavigationBar from "../components/NavigationBar";
import BodyMonth from "../components/BodyMonth";

const MonthPage = () => {
  return (
    <div>
      <NavigationBar />
      <BodyMonth />
    </div>
  );
};

export default MonthPage;
